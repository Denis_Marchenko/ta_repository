﻿#include <iostream>
#include <vector>
using namespace std;

vector<vector<int> > g;
vector<bool> used;
vector<int> comp;

void dfs(int v)
{
	used[v] = true;
	for (int i = 0; i < g[v].size(); ++i)
	{
		int to = g[v][i];
		if (!used[to])
			dfs(to);
	}
	comp.push_back(v);
}

int main(void)
{
	int n, m;
	cin >> n >> m;
	g.resize(n);
	used.resize(n);
	for (int i = 0, a, b; i < m; ++i)
	{
		cin >> a >> b;
		--a, --b;
		g[a].push_back(b);
		g[b].push_back(a);
	}
	for (int i = 0; i < g.size(); ++i)
	{
		if (!used[i])
		{
			comp.clear();
			dfs(i);
			cout << comp.size() << endl;
			for (int j = 0; j < comp.size(); ++j)
				cout << comp[j] << ' ';
			cout << endl;
		}
	}
	return 0;
}