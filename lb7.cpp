﻿#include "pch.h"
#include <iostream>
#include <vector>

using  namespace std;

int make_set(int i, vector <int> &data) {
	return data[i] = i;
}

int find_set(int i, vector <int> &data) {
	return data[i];
}

int union_set(int i, int j, vector <int> &data) {
	int buf_1 = find_set(i, data);
	int buf_2 = find_set(j, data);
	if (buf_1 == buf_2) {
		return 0;
	}
	int m = min(buf_1, buf_2);
	for (int d = 1; d <= data.size(); ++d) {
		if (data[d] == buf_1 || data[d] == buf_2) {
			data[d] = m;
		}
	}
}

int main() {
	int menu;
	int i;
	int j;
	vector <int> data = { 0, 1 ,2, 2, 2, 5, 1, 2, 1, 2 };
	cout << " Add new data enter 1 \t Untile mass 2 \t sucher x 3";
	cin >> menu;
	switch (menu) {
	case 1:
		cout << "What element you want to add \t";
		cin >> i;
		cout << "Result is :\t" << make_set(i, data);
		break;
	case 2:
		cout << "Enter representative num 1: \t";
		cin >> i;
		cout << "Enter representative num 2: \t";
		cin >> j;
		union_set(i, j, data);
		for (int v = 1; v < data.size(); ++v) {
			cout << v << " is " << data[v] << endl;
		}
		break;
	case 3:
		cout << "Enter your number :";
		cin >> i;
		cout << "Representative of your element is : \t" << find_set(i, data);;
		break;
	}
}