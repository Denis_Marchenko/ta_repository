﻿#include <iostream>
#include <cstdlib>

using namespace std;

const int N = 5;

int main()
{
	int mass[N], max, min;

	cout << "Elements: |";
	for (int r = 0; r < N; r++)
	{
		mass[r] = rand() % 99;
		cout << mass[r] << "|";
	}
	cout << endl;

	max = mass[0];
	
	for (int r = 1; r < N; r++)
	{
		if (max < mass[r]) max = mass[r];
	}
	cout << "Max: " << max << endl;

	return 0;
}