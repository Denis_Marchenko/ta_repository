﻿#include <iostream>

void quickSortR(int *a, int size) {

	int i = 0, j = size;
	int temp, p;

	p = a[size >> 1];

	do {
		while (a[i] < p) i++;
		while (a[j] > p) j--;

		if (i <= j) {
			temp = a[i]; a[i] = a[j]; a[j] = temp;
			i++; j--;
		}
	} while (i <= j);

	if (j > 0) quickSortR(a, j);
	if (size > i) quickSortR(a + i, size - i);
}

int main()
{
	int size = 4;
	std::cout << "Size is 4";
	int *mass = new int[size];
	std::cout << "Enter elements (only integer) :";
	for (int q = 0; q < size; q++) {
		std::cin >> mass[q];
	}
	quickSortR(mass, size);
	std::cout << "quiksort is work :\t";
	for (int i = 0; i < size; i++)
		std::cout << mass[i] << " ";

	std::cout << std::endl;

	return 0;
}